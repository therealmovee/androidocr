package com.example.mobilecomputing;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * The type Database.
 */
class Database extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "database.db";

    private static final int DATABASE_VERSION = 1;

    private static final String table_name = "documents_table";

    private static final String document_title = "document_title";

    private static final String document_text = "document_text";

    /**
     * Instantiates a new Database.
     *
     * @param context the context
     */
    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTableQuery = "CREATE TABLE IF NOT EXISTS " + table_name
                + " (id INTEGER PRIMARY KEY, " + document_title + " TEXT, " + document_text + " TEXT)";

        try {
            db.execSQL(createTableQuery);
        } catch(SQLiteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String deleteTableQuery = "DROP TABLE IF EXISTS " + table_name;
        db.execSQL(deleteTableQuery);
    }

    /**
     * Add document.
     *
     * @param documentTitle the document title
     * @param documentText  the document text
     */
    public void addDocument(String documentTitle, String documentText) {
        SQLiteDatabase database = this.getWritableDatabase();
        onCreate(database);
        ContentValues contentValues = new ContentValues();
        contentValues.put(document_title, documentTitle);
        contentValues.put(document_text, documentText);

        database.insert(table_name, null, contentValues);
    }

    /**
     * Gets rows.
     *
     * @return the rows
     */
    public int getRows() {
        SQLiteDatabase database = this.getReadableDatabase();
        onCreate(database);
        Cursor cursor = database.rawQuery("SELECT * FROM " + table_name, null);

        int rows = cursor.getCount();
        cursor.close();

        return rows;
    }

    /**
     * Gets title.
     *
     * @param index the index
     * @return the title
     */
    public String getTitle(int index) {
        SQLiteDatabase database = this.getReadableDatabase();
        onCreate(database);
        String selectQuery = "SELECT " + document_title + " FROM " + table_name + " WHERE id = \"" + index + "\"";

        Cursor cursor = database.rawQuery(selectQuery, null);

        cursor.moveToFirst();
        String title = cursor.getString(cursor.getColumnIndex(document_title));

        cursor.close();

        return title;
    }

    /**
     * Gets text.
     *
     * @param index the index
     * @return the text
     */
    public String getText(int index) {
        SQLiteDatabase database = this.getReadableDatabase();
        onCreate(database);
        String selectQuery = "SELECT " + document_text + " FROM " + table_name + " WHERE id = \"" + index + "\"";

        Cursor cursor = database.rawQuery(selectQuery, null);

        cursor.moveToFirst();
        String text = cursor.getString(cursor.getColumnIndex(document_text));

        cursor.close();

        return text;
    }

    /**
     * Delete a document from the database.
     *
     * @param documentTitle the document title
     */
    public void flushDocument(String documentTitle) {
        SQLiteDatabase database = this.getWritableDatabase();
        onCreate(database);
        database.delete(table_name, document_title + " = ?", new String[] {documentTitle});
    }

    /**
     * Delete all contents of the database.
     */
    public void flushDatabase() {
        SQLiteDatabase database = this.getWritableDatabase();
        onCreate(database);
        String deleteTableQuery = "DROP TABLE IF EXISTS " + table_name;
        database.execSQL(deleteTableQuery);
    }
}
