package com.example.mobilecomputing;

import android.Manifest;
import android.app.Service;
import android.content.DialogInterface;
import android.content.pm.PackageManager;

import android.graphics.Color;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.vision.CameraSource;

import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import java.io.IOException;
import java.util.Locale;

/**
 * The type Reader activity.
 */
public class ReaderActivity extends AppCompatActivity {

    private Button saveButton = null;
    private Button discardButton = null;

    private CameraSource cameraSource = null;
    private SurfaceView cameraHolder = null;
    private TextView detectedText = null;
    private TextToSpeech textToSpeech = null;
    private Database database = null;
    private SensorManager sensorManager = null;
    private Sensor sensor = null;

    private static final int REQUEST_CAMERA_PERMISSION = 100;
    private static final float voicePitch = 0.9f;
    private static final float voiceSpeed = 1.0f;
    private boolean lockDetection = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reader);

        // Button Initialization
        saveButton = findViewById(R.id.saveButton);
        discardButton = findViewById(R.id.discardButton);
        cameraHolder = findViewById(R.id.surfaceView);
        database = new Database(this);
        sensorManager = (SensorManager) getSystemService(Service.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

        saveButton.setEnabled(false);
        discardButton.setEnabled(false);

        detectedText = findViewById(R.id.detectedText);

        // Displaying usage instructions to the User
        displayInstructions();

        // Setting up TextToSpeech
        textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    textToSpeech.setLanguage(Locale.ENGLISH);
                    textToSpeech.setSpeechRate(voiceSpeed);
                    textToSpeech.setPitch(voicePitch);
                }
            }
        });

        detectedText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detectedText.getText() == "")
                    return;

                saveButton.setEnabled(true);
                discardButton.setEnabled(true);

                lockDetection = true;
                detectedText.setTextColor(Color.RED);
                textToSpeech.speak(detectedText.getText().toString(), TextToSpeech.QUEUE_FLUSH, null, null);
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ReaderActivity.this);
                builder.setTitle("Enter a Title");

                final EditText inputTitle = new EditText(ReaderActivity.this);
                inputTitle.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(inputTitle);

                builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (inputTitle.getText().toString().equals(""))
                            return;

                        String title = inputTitle.getText().toString();
                        String text = detectedText.getText().toString();

                        database.addDocument(title, text);

                        AlertDialog.Builder builder2 = new AlertDialog.Builder(ReaderActivity.this);
                        builder2.setMessage("Successfully stored the text in the database.");
                        builder2.setTitle("Storing Document");

                        builder2.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                        builder2.show();

                        lockDetection = false;
                        detectedText.setTextColor(Color.WHITE);
                        saveButton.setEnabled(false);
                        discardButton.setEnabled(false);
                    }
                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builder.show();
            }
        });

        discardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveButton.setEnabled(false);
                discardButton.setEnabled(false);

                if (textToSpeech != null) {
                    textToSpeech.stop();
                }

                detectedText.setTextColor(Color.WHITE);
                detectedText.setText("");
                lockDetection = false;
            }
        });

        buildCamera();
    }

    private void buildCamera() {
        // Building the CameraSource
        TextRecognizer textRecognizer = new TextRecognizer.Builder(getApplicationContext()).build();

        // Checking if the textRecognizer is operational
        if (!textRecognizer.isOperational()) {
            Log.i("Coursework", "Dependencies missing.");
        } else {
            // Building the cameraSource
            cameraSource = new CameraSource.Builder(getApplicationContext(), textRecognizer)
                    .setFacing(CameraSource.CAMERA_FACING_BACK)
                    .setRequestedPreviewSize(1280, 1024)
                    .setRequestedFps(15.0f)
                    .setAutoFocusEnabled(true)
                    .build();

            // Adding the Camera Source into the Surface View
            cameraHolder.getHolder().addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder holder) {
                    // Checking if Camera Permission is granted
                    try {
                        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            // Request Camera Permission
                            ActivityCompat.requestPermissions(ReaderActivity.this, new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
                            return;
                        }

                        // Starting the cameraSource
                        cameraSource.start(cameraHolder.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                }

                @Override
                public void surfaceDestroyed(SurfaceHolder holder) {
                    // Stopping the cameraSource
                    cameraSource.stop();
                }
            });

            // Starting the Detecting Processor
            textRecognizer.setProcessor(new Detector.Processor<TextBlock>() {

                @Override
                public void release() {
                    detectedText.setText("");
                }

                @Override
                public void receiveDetections(final Detector.Detections<TextBlock> detections) {
                    final SparseArray<TextBlock> textBlocks = detections.getDetectedItems();

                    if (!lockDetection) {

                        detectedText.post(new Runnable() {
                            @Override
                            public void run() {
                                if (textBlocks.size() != 0) {
                                    StringBuilder stringBuilder = new StringBuilder();

                                    for (int i = 0; i < textBlocks.size(); i++) {
                                        TextBlock textBlock = textBlocks.valueAt(i);

                                        stringBuilder.append(textBlock.getValue());
                                        stringBuilder.append("\n");
                                    }

                                    // Preventing the Lines from exceeding the available screen size
                                    if (countStringLines(stringBuilder) <= 9) {
                                        detectedText.setText(stringBuilder.toString());
                                    } else {
                                        detectedText.setText(trimStringLines(stringBuilder).toString());
                                    }

                                } else {
                                    detectedText.setText("");
                                }

                            }
                        });
                    }

                }

            });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            try {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(ReaderActivity.this, new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
                    return;
                }

                cameraSource.start(cameraHolder.getHolder());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (textToSpeech != null) {
            textToSpeech.stop();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
    }

    private int countStringLines(StringBuilder stringBuilder) {
        int count = 0;
        String[] lines = stringBuilder.toString().split("\n");

        for (String string : lines) {
            ++count;
        }

        return count;
    }

    private StringBuilder trimStringLines(StringBuilder stringBuilder) {
        String[] lines = stringBuilder.toString().split("\n");
        StringBuilder newStringBuilder = new StringBuilder();

        for (int i = 0; i < lines.length; i++) {
            if (i < 9) {
                newStringBuilder.append(lines[i]);
            } else {
                break;
            }
        }

        return newStringBuilder;
    }

    private void displayInstructions() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ReaderActivity.this);
        builder.setMessage("Instructions: \n\nTo detect any form of text, simply place the camera above the desired text until the correct text appears on the screen (sometimes it may take a while). \n\nOnce the correct text is displayed on the screen, you can tap on it which will lock it onto the screen and make it turn red. Upon locking the text on the screen, you can either save it as a document in the database or discard it. To save it as a document, a title must be entered which will be used for identification purposes. \n\nNote: The Text Recognizer works best on text that has a decent text size and is clear. If there are a lot of text elements on the screen, the text recognizer might sometimes produce a confusing output.");
        builder.setTitle("Usage Instructions");

        builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.show();
    }
}
