package com.example.mobilecomputing;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DocumentActivity extends AppCompatActivity {

    /**
     * Database containing all the documents
     */
    private Database database = null;

    /**
     * Linear Layout that contains all the elements of this Activity
     */
    private LinearLayout linearLayout = null;

    /**
     * Index of the Document in the Table of the Database
     */
    private int documentIndex = -1;

    /**
     * Method that constructs this Activity upon creation.
     *
     * @param savedInstanceState The saved instance of this Activity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document);

        linearLayout = findViewById(R.id.documentLinearLayout);
        database = new Database(this);

        // Getting the Extra data sent from the Previous Activity
        Bundle documentData = getIntent().getExtras();

        // Checking if the data were sent successfully
        if (documentData == null) {
            return;
        } else {
            documentIndex = documentData.getInt("index");
        }

        buildInterface();
    }

    /**
     * Method that builds the User Interface of this Activity
     */
    private void buildInterface() {
        // Checking if the document index is invalid.
        if (documentIndex == -1)
            return;

        // Button that contains the Title of the Document
        Button titleButton = new Button(this);
        titleButton.setText(database.getTitle(documentIndex));
        titleButton.setTextSize(25);
        titleButton.setBackground(ContextCompat.getDrawable(this, R.drawable.button_selector));
        linearLayout.addView(titleButton);

        // TextView used for Spacing
        TextView spacing = new TextView(this);
        linearLayout.addView(spacing);

        // TextView used to display information
        TextView informationTextView = new TextView(this);
        informationTextView.setText(this.getString(R.string.text_contents));
        informationTextView.setGravity(Gravity.CENTER);
        linearLayout.addView(informationTextView);

        // Button that contains the Text of the Document
        Button textButton = new Button(this);
        textButton.setText(database.getText(documentIndex));
        textButton.setGravity(Gravity.START);
        textButton.setBackground(ContextCompat.getDrawable(this, R.drawable.button_selector));
        linearLayout.addView(textButton);

        // TextView used for Spacing
        TextView spacing2 = new TextView(this);
        linearLayout.addView(spacing2);

        // Button used to delete the Document from the Database
        Button deleteButton = new Button(this);
        deleteButton.setText(this.getString(R.string.delete_document));
        deleteButton.setBackground(ContextCompat.getDrawable(this, R.drawable.button_selector));
        linearLayout.addView(deleteButton);

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Notifying the user that the document was deleted.
                AlertDialog.Builder builder = new AlertDialog.Builder(DocumentActivity.this);
                builder.setMessage("Are you sure you would like to delete this document?");
                builder.setTitle("Document Deletion");

                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Deleting the document from the Database
                        database.flushDocument(database.getTitle(documentIndex));

                        // Returning back to Database Activity
                        Intent intent = new Intent(getApplicationContext(), DatabaseActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });

                builder.setNeutralButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builder.show();
            }
        });
    }
}
