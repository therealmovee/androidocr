package com.example.mobilecomputing;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Database activity.
 */
public class DatabaseActivity extends AppCompatActivity {

    private Database database = null;

    private LinearLayout linearLayout = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database);

        database = new Database(this);
        linearLayout = findViewById(R.id.databaseLinearLayout);

        buildInterface();
    }

    /**
     * Method that builds the UI Interface of the Database Activity
     */
    private void buildInterface() {
        TextView textView = new TextView(this);
        textView.setTextSize(20);

        // Checking if there are any documents.
        if (database.getRows() == 0) {
            textView.setText(this.getString(R.string.no_documents_available));
            linearLayout.addView(textView);
        } else {
            Button deleteButton = new Button(this);
            deleteButton.setText("Delete All Documents");
            deleteButton.setBackground(ContextCompat.getDrawable(this, R.drawable.button_selector));

            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Notifying the user that the document was deleted.
                    AlertDialog.Builder builder = new AlertDialog.Builder(DatabaseActivity.this);
                    builder.setMessage("Are you sure you would like to delete all the documents?");
                    builder.setTitle("Document Deletion");

                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Deleting the document from the Database
                            database.flushDatabase();

                            // Reloading the Database Activity
                            Intent intent = new Intent(getApplicationContext(), DatabaseActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });

                    builder.setNeutralButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    builder.show();
                }
            });

            linearLayout.addView(deleteButton);

            textView.setText(this.getString(R.string.saved_documents_information));
            linearLayout.addView(textView);

            final List<Button> buttonList = new ArrayList<>();

            for (int index = 1; index <= database.getRows(); index++) {
                TextView spacing = new TextView(this);
                final Button button = new Button(this);
                button.setBackground(ContextCompat.getDrawable(this, R.drawable.button_selector));
                button.setText(database.getTitle(index));

                // Adding spacing between each button
                linearLayout.addView(spacing);
                linearLayout.addView(button);

                buttonList.add(button);

                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Getting button index
                        int buttonIndex = -2;

                        for (int i = 0; i < buttonList.size(); i++) {
                            if (buttonList.get(i) == button) {
                                buttonIndex = i + 1;
                                break;
                            }
                        }

                        // Checking if the buttonIndex was not found.
                        if (buttonIndex == -1)
                            return;

                        Intent intent = new Intent(getApplicationContext(), DocumentActivity.class);
                        intent.putExtra("index", buttonIndex);

                        startActivity(intent);
                        finish();
                    }
                });
            }
        }
    }
}
